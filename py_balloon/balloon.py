import pygame
import numpy as np

GREEN = (20, 255, 140)
GREY = (210, 210 ,210)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)
PURPLE = (255, 0, 255)
BLACK = (0, 0, 0)
TILESIZE = 32
FPS = 30
PLAYERSIZE = (80, 80)

class gameMap(object):
    """
    THe class for the map data
    """
    def __init__(self, level=1):
        """
        Initialise the class
        """
        self.level = level
        self.map_data = []
    
    def load_data(self):
        """
        Import the map data from file
        """
        filename = 'py_balloon/resources/{}.txt'.format(self.level)
        self.map_data = [row.strip() for row in open(filename)]


class balloon_sprite(pygame.sprite.Sprite):
    """
    The balloon sprite class
    """
    def __init__(self, width, height):
        """
        Initialise the class
        """
        super().__init__()

        balloon_centre_orig = pygame.image.load('py_balloon/resources/balloon-centre.png').convert_alpha()
        #balloon_left1_orig = pygame.image.load('py_balloon/resources/balloon-left1.png').convert_alpha()
        #balloon_left2_orig = pygame.image.load('py_balloon/resources/balloon-left2.png').convert_alpha()
        balloon_right1_orig = pygame.image.load('py_balloon/resources/balloon-right1.png').convert_alpha()
        balloon_right2_orig = pygame.image.load('py_balloon/resources/balloon-right2.png').convert_alpha()

        balloon_burst1_orig = pygame.image.load('py_balloon/resources/balloon-burst1.png').convert_alpha()
        balloon_burst2_orig = pygame.image.load('py_balloon/resources/balloon-burst2.png').convert_alpha()

        balloon_centre= pygame.transform.scale(balloon_centre_orig, (width, height))
        
        balloon_right1= pygame.transform.scale(balloon_right1_orig, (width, height))
        balloon_right2= pygame.transform.scale(balloon_right2_orig, (width, height))
        balloon_left1= pygame.transform.flip(balloon_right1, True, False)
        balloon_left2= pygame.transform.flip(balloon_right2, True, False)

        balloon_burst1= pygame.transform.scale(balloon_burst1_orig, (width, height))
        balloon_burst2= pygame.transform.scale(balloon_burst2_orig, (width, height))
        
        # explosion frames
        self.balloon_explode = []
        for x in range(64):
            exp_frame = pygame.image.load('py_balloon/resources/explosion/image_part_{:03.0f}.png'.format(x+1))
            self.balloon_explode.append(pygame.transform.scale(exp_frame, (width * 2, height * 2)))

        self.balloon = (balloon_centre, balloon_left1, balloon_left2, balloon_left1, balloon_centre, balloon_right1, balloon_right2, balloon_right1)
        self.balloon_burst = (balloon_burst1, balloon_burst2)
        self.image = self.balloon[0]
        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)
        self.balloon_frame = 0
        self.balloon_burst_frame = 0
        self.balloon_explode_frame = 0

    def animate(self, player):
        """
        Update the balloon animation frames
        """
        if player.is_burst is False:
            # Change the balloon frame every 9 counts
            if self.balloon_frame > 63 + 8:
                self.balloon_frame = 0
            self.image = self.balloon[self.balloon_frame//9]
            self.balloon_frame += 1
        else:
            # THe ballon has burst and is falling
            if player.exploding is False:
                if self.balloon_burst_frame == 8:
                    self.balloon_burst_frame = 0
                self.image = self.balloon_burst[self.balloon_burst_frame//4]
                self.balloon_burst_frame += 1
            else:
                if self.balloon_explode_frame < 64:
                    self.image = self.balloon_explode[self.balloon_explode_frame]
                    self.balloon_explode_frame +=1
                else:
                    player.died = True
        self.mask = pygame.mask.from_surface(self.image)



class wall(pygame.sprite.Sprite):
    """
    The wall sprite class
    """
    def __init__(self, width, height):
        super().__init__()
        star_orig = pygame.image.load('py_balloon/resources/star.png').convert_alpha()
        star = pygame.transform.scale(star_orig, (width, height))
        self.image = star
        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)

class goal(pygame.sprite.Sprite):
    """
    The Goal
    This is 3 X 2 Tiles
    """
    def __init__(self, width, height):
        super().__init__()
        
        self.image = pygame.Surface([width, height])
        self.image.fill(WHITE)
        self.image.set_colorkey(WHITE)
        self.colour_list = []
        self.width = width
        self.height = height
        self.rect = self.image.get_rect()
        for x in range(5):
            colour = list(np.random.choice(range(256), size=3))
            self.colour_list.append(colour)

        
    def cycle_colours(self):
        """
        Redraws the rectanges with cycled colours
        """
        pos = self.rect
        self.image.fill(WHITE)
        self.image.set_colorkey(WHITE)
        for x, colour in enumerate(self.colour_list):
            pygame.draw.rect(self.image, colour, [(x * 4), (x * 4), self.width - (x * 8), self.height - (x * 8)])
        move_colour = self.colour_list.pop()
        self.colour_list.insert(0,move_colour)
        self.rect = pos




class player(object):
    """
    THe player class
    """
    def __init__(self, x, y, height, width):
        """
        Initialise the player class
        """
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 4
        self.is_burst = False
        self.exploding = False
        self.died = False
        self.balloonsprite = balloon_sprite(self.width, self.height)

    def draw(self, win):
        """
        Draw the balloon
        """
        self.balloonsprite.animate(self)
        self.balloonsprite.rect.x = self.x
        self.balloonsprite.rect.y = self.y


class game(object):
    """
    The main Game class
    """
    def __init__(self):
        """
        Init the class and load sprite objects
        """
        pygame.init()
        self.player_height = PLAYERSIZE[1]
        self.player_width = PLAYERSIZE[0]
        self.win = pygame.display.set_mode((1024,768))
        self.full_screen = False
        pygame.display.set_caption("Py Balloon")
        self.clock = pygame.time.Clock()
        self.all_sprites_list = pygame.sprite.Group()
        self.wall_tiles_list = pygame.sprite.Group()
        self.goal_tiles_list = pygame.sprite.Group()
        self.player_sprites_list = pygame.sprite.Group()
        self.bonus_tiles_list = pygame.sprite.Group()
        self.level = 1
        self.lives = 3
        self.burst_sound = pygame.mixer.Sound('py_balloon/resources/sounds/burst.wav')
        self.explode_sound = pygame.mixer.Sound('py_balloon/resources/sounds/explosion.wav')
        self.scream_sound = pygame.mixer.Sound('py_balloon/resources/sounds/scream.wav')
        self.scream_sound.set_volume(0.3)
        self.blow_sound = pygame.mixer.Sound('py_balloon/resources/sounds/blow.wav')
        self.bonus_sound = pygame.mixer.Sound('py_balloon/resources/sounds/bonus.wav')
        self.level_complete_sound = pygame.mixer.Sound('py_balloon/resources/sounds/level-complete.wav')
        self.lives_text = ''
        self.level_text = ''
        self.player_text = ''
        self.bonus_countdown_text = ''
        self.score_text = ''
        self.score = 0
        self.font = pygame.font.Font('py_balloon/resources/balloon-font.ttf', TILESIZE -6)
        self.goal_colours = 0
        self.bonus_countdown = 999
        self.bonus_countdown_timer = 0

    def draw_map(self):
        """
        Draw the game level map
        """
        for row, tiles in enumerate(self.playermap.map_data):
            for col, tile in enumerate(tiles):
                if tile == '1':
                    wall_tile = wall(TILESIZE, TILESIZE)
                    wall_tile.rect.x = col * TILESIZE
                    wall_tile.rect.y = row * TILESIZE
                    wall_tile.add(self.wall_tiles_list)
                    wall_tile.add(self.all_sprites_list)
                elif tile == 'P':
                    self.player1.x = col * TILESIZE
                    self.player1.y = row * TILESIZE
                elif tile == 'G':
                    goal_tile = goal(TILESIZE * 3, TILESIZE * 2)
                    goal_tile.cycle_colours()
                    goal_tile.rect.x = col * TILESIZE
                    goal_tile.rect.y = row * TILESIZE
                    goal_tile.add(self.goal_tiles_list)
                elif tile == 'B':
                    bonus_tile = goal(TILESIZE * 1.5, TILESIZE * 1.5)
                    bonus_tile.cycle_colours()
                    bonus_tile.rect.x = col * TILESIZE
                    bonus_tile.rect.y = row * TILESIZE
                    bonus_tile.add(self.bonus_tiles_list)

    def reset(self):
        """
        Resets level after death, or fresh start
        """
        pygame.time.wait(1000)
        self.playermap = gameMap(self.level)
        self.player1 = player(100,100,self.player_height, self.player_width)
        self.player_sprites_list.empty()
        self.all_sprites_list.empty()
        self.wall_tiles_list.empty()    
        self.goal_tiles_list.empty()
        self.bonus_tiles_list.empty()
        self.player_sprites_list.add(self.player1.balloonsprite)
        self.playermap.load_data()
        self.draw_map()
        self.playermap.level = self.level
        self.lives_text = 'balloons: {}'.format(self.lives)
        self.level_text = 'level: {}'.format(self.level)
        self.player_text = 'Player 1'
        self.bonus_countdown = 999

    def balloon_sound(self):
        if not pygame.mixer.get_busy():
            self.blow_sound.stop()
            self.blow_sound.play()
    
    def new_game(self):
        """
        Play a game
        """
        while self.lives > 0:
            self.reset()
            
            run = True
            while run:
                self.bonus_countdown_text = '{0:0=3d}'.format(self.bonus_countdown)
                self.score_text = '{0:0=7d}'.format(self.score)
                timer = self.clock.tick(FPS)
                keys = pygame.key.get_pressed()
                if keys[pygame.K_f]:
                    if self.full_screen is False:
                        pygame.display.set_mode((1024,768), pygame.FULLSCREEN)
                        self.full_screen = True
                    else:
                        pygame.display.set_mode((1024,768))
                        self.full_screen = False
                if self.player1.is_burst is False:
                    if keys[pygame.K_LEFT] and self.player1.x > self.player1.vel:
                        self.player1.x -= self.player1.vel
                        self.balloon_sound()
                    if keys[pygame.K_RIGHT] and self.player1.x < 1024 -self.player1.width -self.player1.vel:
                        self.player1.x += self.player1.vel
                        self.balloon_sound()
                    if keys[pygame.K_UP] and self.player1.y > self.player1.vel:
                        self.player1.y -= self.player1.vel
                        self.balloon_sound()
                    if keys[pygame.K_DOWN] and self.player1.y < 768 -self.player1.height -self.player1.vel:
                        self.player1.y += self.player1.vel
                        self.balloon_sound()
                else:
                    
                    if self.player1.y > 768 -(self.player1.height * 2)  -self.player1.vel:
                        if self.player1.exploding is False:
                            self.scream_sound.stop()
                            self.explode_sound.play()
                            self.player1.exploding = True
    
                    else:
                        self.player1.y += ((self.player1.vel ** 2) // 2)
                if self.player1.died:
                    self.lives -= 1
                    run = False
                self.redraw_game()
                collision_list = pygame.sprite.spritecollide(self.player1.balloonsprite, self.wall_tiles_list, False, pygame.sprite.collide_mask)
                if len(collision_list) > 0:
                    if self.player1.is_burst is False:
                        self.clock.tick(10)
                        self.player1.is_burst = True
                        self.blow_sound.stop()
                        self.burst_sound.play()
                        self.scream_sound.play()

                if self.player1.is_burst is False:
                    collision_list = pygame.sprite.spritecollide(self.player1.balloonsprite, self.goal_tiles_list, False, pygame.sprite.collide_mask)
                    if len(collision_list) > 0:
                        self.player_sprites_list.empty()
                        self.redraw_game()
                        self.blow_sound.stop()
                        self.level_complete_sound.play()
                        self.score += self.bonus_countdown
                        self.clock.tick(60)
                        self.lives +=1
                        self.level +=1
                        self.next_flight()
                        run = False
                collision_list = pygame.sprite.spritecollide(self.player1.balloonsprite, self.bonus_tiles_list, False, pygame.sprite.collide_mask)
                if len(collision_list) > 0:
                    self.blow_sound.stop()
                    self.bonus_sound.play()
                    self.score += 100
                    self.bonus_tiles_list.empty()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        run = False
                        self.lives = 0
                        self.reset()
        else:
            print('quitting')
            pygame.quit()

        

    def redraw_game(self):
        """
        Redraw the game screen after update
        """
        self.win.fill(BLACK)
        self.all_sprites_list.update()
        self.all_sprites_list.draw(self.win)
        self.player1.draw(self.win)
        self.player_sprites_list.update()
        self.player_sprites_list.draw(self.win)
        self.goal_tiles_list.update()
        self.goal_tiles_list.draw(self.win)
        self.bonus_tiles_list.update()
        self.bonus_tiles_list.draw(self.win)
        self.draw_text()
        if self.goal_colours >= 15:
            for goal in self.goal_tiles_list:
                goal.cycle_colours()
            for bonus in self.bonus_tiles_list:
                bonus.cycle_colours()
            self.goal_colours = 0
        self.goal_colours += 1
        if self.bonus_countdown_timer == 2:
            if self.bonus_countdown > 0:
                self.bonus_countdown -= 1
            self.bonus_countdown_timer = 0
        self.bonus_countdown_timer += 1
        pygame.display.flip()

    def draw_text(self):
        """
        Draw the text objects on screen
        """
        lives = self.font.render(self.lives_text, True, GREEN)
        level = self.font.render(self.level_text, True, GREEN)
        player = self.font.render(self.player_text + '      ' + self.score_text, True, GREEN)
        bonus_countdown_text = self.font.render(self.bonus_countdown_text, True, WHITE)
        lives_rect = lives.get_rect()
        level_rect = level.get_rect()
        player_rect = player.get_rect()
        bonus_countdown_rect = bonus_countdown_text.get_rect()
        lives_rect.bottomright=(1024, 768)
        level_rect.midbottom = (1024 /2, 768)
        player_rect.bottomleft = (0,768)
        for goal_tile in self.goal_tiles_list:
            bonus_countdown_rect.center = goal_tile.rect.center
        self.win.blit(lives, lives_rect)
        self.win.blit(level, level_rect)
        self.win.blit(player, player_rect)
        self.win.blit(bonus_countdown_text, bonus_countdown_rect)

    def next_flight(self):
        """
        Display a scree preparing for next flight
        After completing a level
        """
        self.win.fill(BLACK)
        preparetext = "Prepare for Next Flight!"
        prepare = self.font.render(preparetext, True, YELLOW)
        prepare_rect = prepare.get_rect()
        prepare_rect.center = (1024 /2, 768 /2)
        self.win.blit(prepare, prepare_rect)
        pygame.display.flip()
        self.clock.tick(4000)
        #pygame.time.wait(4000)

if __name__ == "__main__":
    newgame = game()
    newgame.new_game()